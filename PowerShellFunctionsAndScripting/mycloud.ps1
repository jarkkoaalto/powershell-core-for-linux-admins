function set-mycloud {
	Import-Module AWSPowerShell.NetCore
	Set-AWSCredentials -ProfileName awsLA1
}

function update-mycloud{
	$buckets = Get-S3Bucket
	$current = Get-ChildIten /buckets31 -rec|ForEach-Object -Process {$_.FullName}

	foreach ($f in $current) {
		foreach($b in $buckets){
			Write-S3Object -BucketName $b.BucketName -File $f -CannedACLName public-read	
		}
	}
}

